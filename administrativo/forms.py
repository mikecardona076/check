from django import forms 
from .models import *
from splitjson.widgets import SplitJSONWidget


class check_form(forms.ModelForm):
    
    class Meta:
        model = Check

        fields = "__all__"

        labels = {
            'slug_check' : "Motivo"

        }

        widgets = {
            "user" : forms.HiddenInput(), 
            'check' : forms.HiddenInput()

        }
