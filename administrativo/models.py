from django.db import models
from django.contrib.auth.models import User
import datetime

# Create your models here.

admin_choices  = [
    ("Hora de entrada", "Hora de entrada"),
    ("Hora de salida", "Hora de salida"),
    ("Hora de comida", "Hora de comida"),
    ("otra", "otra"),
]
class Check(models.Model):

    user = models.ForeignKey(User, on_delete=models.CASCADE)

    check = models.DateTimeField()

    status = models.BooleanField(default=True)

    slug_check = models.CharField(max_length = 40, choices=admin_choices)

    check_day = models.DateTimeField(auto_now_add=True)
