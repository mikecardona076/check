from django.urls import path
from . import views

urlpatterns = [

    path("", views.Index.as_view(), name="index"),
    path("Vercheck/<int:pk>", views.Informacion_check.as_view(), name="Vercheck"),
    path("Editarcheck/<int:pk>", views.Editar_check.as_view(), name="Editarcheck"),
    path("Eliminarcheck/<int:pk>", views.Eliminar_check.as_view(), name="Eliminarcheck"),



]