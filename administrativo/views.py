from django.shortcuts import render
from django.views import generic
from .forms import *
from .models import *
from django.urls import reverse_lazy
from datetime import datetime
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views import generic

# Create your views here.

class Index(LoginRequiredMixin, generic.CreateView):
    template_name = 'index.html'
    model = Check
    form_class = check_form
    success_url = reverse_lazy('index')

    def get(self, request, *args, **kwargs):

        checks = Check.objects.all()

        hora = str(datetime.now())

        form = self.form_class(initial={
            "user": request.user,
            'check' : hora
        })

        return render(request, self.template_name, {'form': form, "checks" : checks })

class Editar_check(LoginRequiredMixin, generic.UpdateView):
    template_name = 'generica.html'
    model = Check
    form_class = check_form
    success_url = reverse_lazy('index')


class Informacion_check(LoginRequiredMixin, generic.DetailView):
    template_name = 'ver.html'
    model = Check
    


#Clase para Eliminar servicio
class  Eliminar_check(LoginRequiredMixin, generic.DeleteView):
    template_name = 'generica.html'
    model = Check
    success_url = reverse_lazy('index')



